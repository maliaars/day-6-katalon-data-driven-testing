<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Checkout Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>a6698794-08be-4e48-bb81-632f1b971733</testSuiteGuid>
   <testCaseLink>
      <guid>2053b7da-e924-4a9d-9da3-58d0a86aabb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC006 - Add Product to Cart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fa78295c-2241-439a-a885-c2371814161b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC007 - Remove Product from Cart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>787e6fbc-e4c6-4817-9c25-a9123a76feda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC008 - Succeed Checkout</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7c44e38c-ffae-4c07-b9dc-b203d43efe3e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TestData_Cust</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>7c44e38c-ffae-4c07-b9dc-b203d43efe3e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>first</value>
         <variableId>c5a6abbf-df1a-4401-8c87-2fb2478c8ff2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7c44e38c-ffae-4c07-b9dc-b203d43efe3e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>last</value>
         <variableId>43459815-a452-4010-9bcf-1fb2aab1ee10</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7c44e38c-ffae-4c07-b9dc-b203d43efe3e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>postal_code</value>
         <variableId>b8827507-5b63-4f73-b615-ef632efb2125</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4cb7412f-4c24-4ac5-80c7-ac62faf0dd90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC009 - Checkout - Empty Postal Code</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>754a563c-892d-49b0-bf25-601e818d168d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC010 - Checkout - Empty Name</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
