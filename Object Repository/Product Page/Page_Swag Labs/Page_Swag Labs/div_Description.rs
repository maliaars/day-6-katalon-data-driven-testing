<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Description</name>
   <tag></tag>
   <elementGuidId>461a9e62-bbce-4c8c-8664-f8c5bbbd1d31</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.cart_desc_label</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='cart_contents_container']/div/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f9be4b52-a746-4e48-aec8-24e56adcad39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cart_desc_label</value>
      <webElementGuid>bc662a16-57e8-418a-9f37-d034907c5906</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Description</value>
      <webElementGuid>14fabc1f-9683-4c03-8e41-d04fe8c91d58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cart_contents_container&quot;)/div[1]/div[@class=&quot;cart_list&quot;]/div[@class=&quot;cart_desc_label&quot;]</value>
      <webElementGuid>6d88ea5f-6664-4a73-aca0-d44b58ff1b42</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='cart_contents_container']/div/div/div[2]</value>
      <webElementGuid>eacebafe-dd74-4e87-9095-5ca1c72f5eb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='QTY'])[1]/following::div[1]</value>
      <webElementGuid>b96b8877-3034-412b-8a4a-62166022b45f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your Cart'])[1]/following::div[5]</value>
      <webElementGuid>09824c6b-3256-4cf7-b9ef-05403817818a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sauce Labs Backpack'])[1]/preceding::div[2]</value>
      <webElementGuid>8fb866b3-24ae-4457-9bd9-68be7f71023b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$29.99'])[1]/preceding::div[4]</value>
      <webElementGuid>8cb9b4fd-d0a2-4bea-aad7-cc528a80d7a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Description']/parent::*</value>
      <webElementGuid>9af7d8a0-7797-483a-961c-2639cd54d504</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[2]</value>
      <webElementGuid>437abd5d-8e4b-4ddd-8c00-2978e81f8568</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Description' or . = 'Description')]</value>
      <webElementGuid>f028f81d-91af-47fb-9dd0-b8f0632c4625</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
